#!/bin/sh

/usr/local/bin/geoipupdate -v -d /usr/share/GeoIP || exit 1
exec /usr/local/bin/mirrorbits daemon -config /etc/mirrorbits.conf
